package Gr11;

/*A string is given as:
Write a program in Java to enter the string. Count and display:
1. The character with the lowest ASCII codes in lower case
2. The character with the highest ASCII codes in lower case
3. The character with the lowest ASCII codes in upper case
4. The character with the highest ASCII codes in upper case*/

import java.util.Scanner;

public class ASCII_Range {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Enter the string:");
        String s = in.nextLine();
        ascii_range(s);
    }

    static void ascii_range(String s) {
        char lcL = 255, lcH = 0, ucL = 255, ucH = 0;
        for (int i = 0; i < s.length(); i++) {
            char ch = s.charAt(i);
            if (Character.isLowerCase(ch)) {
                if (ch < lcL) {
                    lcL = ch;
                }
                if (ch > lcH) {
                    lcH = ch;
                }
            } else if (Character.isUpperCase(ch)) {
                if (ch < ucL) {
                    ucL = ch;
                }
                if (ch > ucH) {
                    ucH = ch;
                }
            }
        }
        System.out.println("The character with lowest ASCII code in lower case: " + lcL);
        System.out.println("The character with highest ASCII code in lower case: " + lcH);
        System.out.println("The character with lowest ASCII code in upper case: " + ucL);
        System.out.println("The character with highest ASCII code in upper case: " + ucH);
    }
}
