package Gr11;

/*An Armstrong number is a number whose sum of the cube of
the digits is equal to the original number
Given two positive integers m and n, where m < n, write a
program to determine how many armstrong integers are there in
the range between m and n (both inclusive) and output them.
The input contains two positive integers m and n where m < 5000
and n < 5000. Display the number of Armstrong integers in the
specified range along with their values*/

import java.util.Scanner;

public class Armstrong {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int m, n, c, i;
        do { // Do-While loop that repeats until user enters a correct input
            System.out.print("Enter the lower limit (m): ");
            m = in.nextInt();
            System.out.print("Enter the upper limit (n): ");
            n = in.nextInt();
            if (m >= n || m >= 5000 || n >= 5000 || m < 1) {
                System.out.print("Invalid Input! Enter the values again\n\n");
            }
        } while (m >= n || m >= 5000 || n >= 5000 || m < 1);
        System.out.println("The Armstrong Numbers are: ");
        for (i = m, c = 0; i <= n; i++) {
            if (isArmstrong(i)) {
                ++c;
                System.out.print(i + " = ");
                for (int ci = reverse(i); ci > 0; ci /= 10) {
                    System.out.print((int) (Math.pow(ci % 10, 3)) + "+");
                }
                System.out.print("\b\n");
            }
        }
        if (c == 0) System.out.println("None!  ");
        System.out.println("Frequency: " + c); //To remove the extra pluses (+) at the end and display the frequency
    }

    static boolean isArmstrong(int n) {
        return n == digitsumofcubes(n);
    }

    static int digitsumofcubes(int n) {
        int sumofcubes = 0;
        for (; n > 0; n /= 10) {
            sumofcubes += (int) (Math.pow(n % 10, 3));
        }
        return sumofcubes;
    }

    static int reverse(int n) { // Function to reverse n and return the reversed number
        int reverse = 0;
        for (; n > 0; n /= 10) {
            reverse = reverse * 10 + (n % 10);
        }
        return reverse;
    }
}