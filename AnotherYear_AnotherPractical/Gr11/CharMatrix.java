package Gr11;

/*Given a square matrix M[][] of order 'n'. The maximum value possible
for 'n' is 10. Accept three different characters from the keyboard and
fill the array according to the output shown in the examples. If the
value of n exceeds 10 then an appropriate message should be
displayed.*/

import java.util.Scanner;

public class CharMatrix {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n, i, j;
        do { // Do-While loop that repeats until user enters a correct input
            System.out.print("Enter the order (n): ");
            n = in.nextInt();
            if (n < 1 || n > 10) {
                System.out.print("Invalid Input! Enter the values again\n\n");
            }
        } while (n < 1 || n > 10);
        System.out.print("Enter the first character: ");
        char a = in.next().charAt(0);
        System.out.print("Enter the second character: ");
        char b = in.next().charAt(0);
        System.out.print("Enter the third character: ");
        char c = in.next().charAt(0);
        char[][] k = new char[n][n];
        for (i = 0; i < n; i++) {
            for (j = 0; j < n; j++) {
                if (i == j || (i + j) == n - 1) {
                    k[i][j] = c;
                } else {
                    if ((i + j) < n - 1) {
                        if (i > j) k[i][j] = b;
                        else k[i][j] = a;
                    } else {
                        if (i > j) k[i][j] = a;
                        else k[i][j] = b;
                    }
                }
            }
        }

        System.out.println("The pattern is:");
        for (i = 0; i < n; i++) {
            for (j = 0; j < n; j++) {
                System.out.print(k[i][j] + "\t");
            }
            System.out.println();
        }
    }
}
