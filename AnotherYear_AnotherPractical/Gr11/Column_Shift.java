package Gr11;

/*Write a program in Java to enter natural numbers in a double
dimensional array mxn (where m is the number of rows and n is the
number of columns). Shift the elements of 4th column into the 1
st column, the elements of 1st column into the 2nd column and so on.
Display the new matrix.*/

import java.util.Scanner;

public class Column_Shift {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int m, n;
        do {
            System.out.print("Enter number of rows (m): ");
            m = in.nextInt();
            System.out.print("Enter number of columns (n): ");
            n = in.nextInt();
            if ((m < 1 || n < 1)) {
                System.out.println("Invalid Input! Enter the values again");
            }
        } while (m < 1 || n < 1);
        int[][] a = new int[m][n];
        int[][] na = new int[m][n];
        dda_input(a, m, n, "Enter the elements:\n");
        dda_display(a, m, n, "Original Array:\n");
        for (int j = 0; j < n; j++) {
            int col = j + 1;
            if (col == n) {
                col = 0;
            }
            for (int i = 0; i < m; i++) {
                na[i][col] = a[i][j];
            }
        }
        dda_display(na, m, n, "New Shifted Array:\n");
    }

    static void dda_input(int[][] a, int r, int c, String display) {
        Scanner in = new Scanner(System.in);
        System.out.print(display);
        for (int i = 0; i < r; i++) {
            for (int j = 0; j < c; j++) {
                a[i][j] = in.nextInt();
            }
        }
    }

    static void dda_display(int[][] a, int r, int c, String display) {
        System.out.print(display);
        for (int i = 0; i < r; i++) {
            for (int j = 0; j < c; j++) {
                System.out.print(a[i][j] + "\t");
            }
            System.out.println();
        }
    }
}
