package Gr11;

/*A composite magic number is a positive integer which is composite as well as a
magic number. Accept two positive integers ‘m’ and ‘n’, where m is less than n as user input. Display the
number of composite magic integers that are in the range between ‘m’ and ‘n’ (both
inclusive) and output them along with the frequency, */

import java.util.Scanner;

public class Composite_Magic {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int m, n, c, i;
        do { // Do-While loop that repeats until user enters a correct input
            System.out.print("Enter the lower limit (m): ");
            m = in.nextInt();
            System.out.print("Enter the upper limit (n): ");
            n = in.nextInt();
            if (m >= n || m < 1) {
                System.out.print("Invalid Input! Enter the values again\n\n");
            }
        } while (m >= n || m < 1);
        System.out.println("The Composite Magic Numbers are: ");
        for (i = m, c = 0; i <= n; i++) {
            if (!(isComposite(i)) && isMagic(i)) {
                ++c;
                System.out.print(i + ", ");
            }
        }
        if (c == 0) System.out.print("None!  ");
        System.out.println("\b\b\nFrequency: " + c); //To remove the extra commas (,) at the end and display the frequency
    }

    static boolean isComposite(int n) { //Function to check whether n is a composite number or not
        int c = 0;
        if (n == 1) {
            return false;
        } else if (n == 4) {
            return true;
        } else {
            for (int i = 2; i < (n / 2); i++) {
                if (n % i == 0) {
                    ++c;
                }
            }
        }
        return c != 0;
    }

    static boolean isMagic(int n) { //Recursive Method to check whether n is a Magic Number or not
        if (n == 1) {
            return true;
        } else if (no_digits(n) == 1) {
            return false;
        } else {
            return isMagic(sumofdigits(n));
        }
    }

    static int sumofdigits(int n) {
        int sumofdigits = 0;
        for (; n > 0; n /= 10) {
            sumofdigits += (n % 10);
        }
        return sumofdigits;
    }

    static int no_digits(int n) {
        n = Math.abs(n);
        for (int i = 1; ; i++) {
            if (n < (int) (Math.pow(10, i))) {
                return i;
            }
        }
    }
}
