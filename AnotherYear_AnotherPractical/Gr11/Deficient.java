package Gr11;

/*Deficient number: In number theory, a deficient number is a number n for
which the sum of divisors σ(n)<2n, or, equivalently, the sum of proper divisors
(or aliquot sum) s(n)<n. The value 2n − σ(n) (or n − s(n)) is called the
number's deficiency.
As an example, divisors of 21 are 1, 3 and 7, and their sum is 11. Because 11
is less than 21, the number 21 is deficient. Its deficiency is 2 × 21 − 32 = 10.
The first few deficient numbers are:
1, 2, 3, 4, 5, 7, 8, 9, 10, 11, 13, 14, 15, 16, 17, 19, 21, 22, 23, 25, 26, 27, 29,
31, 32, 33, .......
Accept two positive integers ‘m’ and ‘n’, where m is less than n as user input. Display the
number of deficient integers that are in the range between ‘m’ and ‘n’ (both inclusive)
and output them along with the frequency, in the format specified below.*/

import java.util.Scanner;

public class Deficient {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int m, n, c, i;
        do { // Do-While loop that repeats until user enters a correct input
            System.out.print("Enter the lower limit (m): ");
            m = in.nextInt();
            System.out.print("Enter the upper limit (n): ");
            n = in.nextInt();
            if (m >= n || m < 1) {
                System.out.print("Invalid Input! Enter the values again\n\n");
            }
        } while (m >= n || m < 1);
        System.out.print("The Deficient Numbers are:\t");
        for (i = m, c = 0; i <= n; i++) {
            if (isDeficient(i)) {
                ++c;
                System.out.print(i + ", ");
            }
        }
        if (c == 0) System.out.print("None!  ");
        System.out.println("\b\b\nFrequency: " + c); //To remove the extra commas (,) at the end and display the frequency
    }

    static boolean isDeficient(int n) {
        return aliquot_sum(n) < n;
    }

    static int aliquot_sum(int n) {
        int S = 0;
        for (int i = 1; i <= (n / 2); i++) {
            S += (n % i == 0) ? i : 0;
        }
        return S;
    }
}
