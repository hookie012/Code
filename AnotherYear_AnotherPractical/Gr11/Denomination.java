package Gr11;

/*A bank intends to design a program to display the denomination of
an input amount, up to 5 digits. The available denomination with the
bank are of rupees 2000, 500, 200, 100, 50, 20, 10 and 1.
Design a program to accept the amount from the user and display
the break-up in descending order of denominations. (i.e., preference
should be given to the highest denomination available) along with the
total number of notes.*/

import java.util.Scanner;

public class Denomination {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Enter the number: ");
        int n = in.nextInt(), rc = 0, da, noda = 0, c, ca, non = 0;
        int[] arr = {1000, 500, 100, 50, 20, 10, 5, 2, 1};
        String temp = "\t";
        for (int c1 = n; c1 > 0; c1 /= 10, noda++) {
            rc = (rc * 10) + (c1 % 10);//loop for reversing the amount and finding the digits
        }
        String[] word = {"ZERO", "ONE", "TWO", "THREE", "FOUR", "FIVE", "SIX", "SEVEN", "EIGHT", "NINE"};
        for (ca = rc, c = 0; ca > 0; ca /= 10, c++) {
            System.out.print(word[ca % 10] + " ");//loop for getting each digit of the amount and then printing them in words.
        }
        if (c != noda) {
            for (int i = 1; i <= (noda - c); i++) {
                System.out.print("ZERO ");
            }
        }
        System.out.println("\nDENOMINATION:");
        for (int cca = n; cca > 0; ) {
            for (int i = 0; i < arr.length; i++, cca %= da, temp = "\t\t") {
                da = arr[i];
                if (cca / da != 0) {
                    non += (cca / da);
                    System.out.println("\t" + da + temp + "x" + "\t" + (cca / da) + " \t=\t " + da * (cca / da));
                }
            }
        }
        System.out.println("Total Number of Notes: " + non);
    }
}