package Gr11;

/*Disarium number
A number is said to be the Disarium number when the sum of its digit raised to the
power of their respective positions is equal to the number itself.*/

import java.util.Scanner;

public class Disarium {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int m, n, i, c;
        do { // Do-While loop that repeats until user enters a correct input
            System.out.print("Enter the lower limit (m): ");
            m = in.nextInt();
            System.out.print("Enter the upper limit (n): ");
            n = in.nextInt();
            if (m >= n || m < 1) {
                System.out.print("Invalid Input! Enter the values again\n\n");
            }
        } while (m >= n || m < 1);
        System.out.println("The Disarium numbers are: ");
        for (i = m, c = 0; i <= n; i++) {
            if (isDisarium(i)) {
                ++c;
                System.out.print(i + ", ");
            }
        }
        if (c == 0) System.out.print("None!  ");
        System.out.println("\b\b\nFrequency: " + c); //To remove the extra commas (,) at the end and display the frequency
    }

    static boolean isDisarium(int n) {
        int S = 0;
        for (int i = 1, cn = reverse(n); cn > 0; cn /= 10, i++) {
            S += (int) (Math.pow((cn % 10), i));
        }
        return S == n;
    }

    static int reverse(int n) { // Function to reverse n and return the reversed number
        int reverse = 0;
        for (; n > 0; n /= 10) {
            reverse = reverse * 10 + (n % 10);
        }
        return reverse;
    }
}
