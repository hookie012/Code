package Gr11;

/*The International Standard Book Number (ISBN) is a unique numeric book
identifier which is printed on every book. The ISBN is based upon a 10-digit
code. The ISBN is legal if:
1 * digit1 + 2 * digit2 + 3 * digit3 + 4 * digit4 + 5 * digit5 + 6 * digit6 + 7 * digit7 + 8 * digit8 +
9 * digit9 + 10 * digit10 is divisible by 11.*/

import java.util.Scanner;

public class ISBN {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        long n;
        int i, S = 0;
        System.out.print("Enter the ISBN Number: ");
        n = in.nextLong();
        if (no_digits(n) != 10) {
            System.out.println("Illegal ISBN");
            System.exit(0);
        }
        for (i = 1; i <= 10; i++, n /= 10) {
            S += i * (n % 10);
        }
        if (S % 11 == 0) {
            System.out.println("Legal ISBN");
        } else {
            System.out.println("Illegal ISBN");
        }
    }

    static int no_digits(long n) {
        n = Math.abs(n);
        for (int i = 1; ; i++) {
            if (n < (int) (Math.pow(10, i))) {
                return i;
            }
        }
    }
}