package Gr11;

import java.util.Scanner;

public class LSoD_N {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        long m, n, i, t;
        do { // Do-While loop that repeats until user enters a correct input
            System.out.print("Enter the value of m: ");
            m = in.nextInt();
            System.out.print("Enter the value of n: ");
            n = in.nextInt();
            if (n >= 100 || m < 100 || m > 10000) {
                System.out.print("Invalid Input! Enter the values again\n\n");
            }
        } while (n >= 100 || m < 100 || m > 10000);
        long[] arr = new long[(int) ((n % 9 == 0) ? (n / 9) : (n / 9) + 1)];
        if (arr.length <= 2) {
            lsod_n(m, n);
        } else {
            for (i = 0; i < (n / 9); i++) {
                arr[(int) i] = 9;
            }
            if (n % 9 != 0) arr[arr.length - 1] = n % 9;
            for (i = arr.length - 1, t = 0; i >= 0; i--) {
                t = t * 10 + arr[(int) i];
            }
            if (t < m) {
                lsod_n(m, n);
            } else {
                System.out.println("The required number is: " + t + "\nThe number of digits is: " + arr.length);
            }
        }
    }

    static void lsod_n(long m, long n) {
        for (long i = m; no_digits(i) <= n; i++) {
            if (sumofdigits(i) == n) {
                System.out.println("The required number is: " + i + "\nThe number of digits is: " + no_digits(i));
                System.exit(0);
            }
        }
    }

    static int sumofdigits(long n) {
        int sumofdigits = 0;
        for (; n > 0; n /= 10) {
            sumofdigits += (n % 10);
        }
        return sumofdigits;
    }

    static int no_digits(long n) {
        n = Math.abs(n);
        for (int i = 1; ; i++) {
            if (n < (int) (Math.pow(10, i))) {
                return i;
            }
        }
    }
}
