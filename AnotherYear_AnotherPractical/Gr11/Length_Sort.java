package Gr11;

/*Read a single sentence which terminates with a full stop (.). The words are to be separated
with a single blank space and are in lower case. Arrange the words contained in the sentence
according to the length of the words in ascending order. If two words are of the same length
then the word occurring first in the input sentence should come first. For both, input and
output the sentence must begin in upper case.*/

import java.util.Scanner;
import java.util.StringTokenizer;

public class Length_Sort {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String s;
        do {
            System.out.print("Enter the sentence: ");
            s = in.nextLine();
            if (s.charAt(s.length() - 1) != '.' || (Character.isLowerCase(s.charAt(0)))) {
                System.out.println("Invalid Input! Sentence should start with upper case letter and end with full stop.");
            }
        } while ((s.charAt(s.length() - 1) != '.') || (Character.isLowerCase(s.charAt(0))));
        String s1 = Character.toLowerCase(s.charAt(0)) + s.substring(1, s.length() - 1);
        StringTokenizer st = new StringTokenizer(s1);
        int wc = st.countTokens();
        String[] a = new String[wc];
        for (int i = 0; i < wc; i++) {
            a[i] = st.nextToken();
        }
        for (int i = 0; i < wc - 1; i++) {
            for (int j = 0; j < wc - i - 1; j++) {
                if (a[j].length() > a[j + 1].length()) {
                    String t = a[j];
                    a[j] = a[j + 1];
                    a[j + 1] = t;
                }
            }
        }
        a[0] = Character.toUpperCase(a[0].charAt(0)) + a[0].substring(1);
        System.out.println("Sorted String:");
        for (int i = 0; i < wc; i++) {
            System.out.print(a[i]);
            if (i == wc - 1) {
                System.out.print(".");
            } else {
                System.out.print(" ");
            }
        }
    }
}
