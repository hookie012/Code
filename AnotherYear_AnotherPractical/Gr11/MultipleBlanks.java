package Gr11;

/*While typing, a typist has created two or more consecutive blank
spaces between the words of a sentence. Write a program in Java to
eliminate multiple blanks between the words by a single blank.*/

import java.util.Scanner;

public class MultipleBlanks {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Enter the string: ");
        System.out.println("Output String:\n" + in.nextLine().trim().replaceAll("\\s+", " "));
    }
}
