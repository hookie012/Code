package Gr11;

/*Write a Program in Java to fill a 2D array with the first 'mxn' prime
numbers, where 'm' is the number of rows and 'n' is the number of
columns.*/

import java.util.Scanner;

public class Prime_Matrix {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n, m;
        do { // Do-While loop that repeats until user enters a correct input
            System.out.print("Enter the number of rows (m): ");
            m = in.nextInt();
            System.out.print("Enter the number of columns (n): ");
            n = in.nextInt();
            if (n < 1 || m < 1) {
                System.out.print("Invalid Input! Enter the values again\n\n");
            }
        } while (n < 1 || m < 1);
        int[][] a = new int[m][n];
        dda_fill(a, m, n, 0, 0, 2, isPrime(2));
        dda_display(a, m, n, "\n");
    }

    static void dda_fill(int[][] a, int m, int n, int i, int j, int k, boolean condition) {
        if (condition) {
            a[i][j] = k;
            j++;
            if (j == n) {
                i++;
                j = 0;
            }
        }
        if (i < m) {
            dda_fill(a, m, n, i, j, ++k, isPrime(k));
        }
    }

    static boolean isPrime(int n) { //Function to check whether n is a prime number or not
        int c = 0;
        if (n == 1 || n == 4) {
            return false;
        } else if (n == 2) {
            return true;
        } else {
            for (int i = 2; i < (n / 2); i++) {
                if (n % i == 0) {
                    ++c;
                }
            }
        }
        return c == 0;
    }

    static void dda_display(int[][] a, int r, int c, String display) {
        System.out.print(display);
        for (int i = 0; i < r; i++) {
            for (int j = 0; j < c; j++) {
                System.out.print(a[i][j] + "\t");
            }
            System.out.println();
        }
    }
}
