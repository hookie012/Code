package Gr11;

/*A prime palindrome integer is a positive integer which is prime
as well as a palindrome. Given two positive integers m and n, where m < n,
write a program to determine how many prime palindrome integers are there
in the range between m and n (both inclusive) and output them. Display the
number of prime palindrome integers in the specified range.*/

import java.util.Scanner;

public class Prime_Palindrome {
    public static void main(String[] args) {
        int m, n;
        do {
            m = input("Enter the lower limit (m): ");
            n = input("Enter the upper limit (n): ");
            if (m >= n)
                System.out.print("Invalid Input! Enter the value(s) again!\n\n");
        } while (m >= n);
        print_PalindromicPrime(m, n);
    }

    static int input(String display) {
        Scanner in = new Scanner(System.in);
        int n;
        do { // Do-While loop that repeats until user enters a correct input
            System.out.print(display);
            n = in.nextInt();
            if (n < 1)
                System.out.print("Invalid Input! Enter the value(s) again!\n\n");
        } while (n < 1);
        return n;
    }

    static void print_PalindromicPrime(int lower, int upper) {
        System.out.println("The Palindromic Prime Numbers are: ");
        int i, c;
        for (i = lower, c = 0; i <= upper; i++)
            if (isPrime(i) && isPalindrome(i))
                if (++c == 1)
                    System.out.print(i);
                else
                    System.out.print(", " + i);
        if (c == 0)
            System.out.println("None!  ");
        else
            System.out.println("\nFrequency: " + c);
    }

    public static boolean isPrime(int n) {
        if (n < 2)
            return false;
        else if (n < 4)
            return true;
        else if (n % 2 == 0 || n % 3 == 0)
            return false;
        for (int i = 5; i * i < n + 1; i += 6)
            if (n % i == 0 || n % (i + 2) == 0)
                return false;
        return true;
    }

    static int reverse(int n) { // Function to reverse n and return the reversed number
        int reverse = 0;
        for (; n > 0; n /= 10) {
            reverse = reverse * 10 + (n % 10);
        }
        return reverse;
    }

    static boolean isPalindrome(int n) { //Function to check whether n is palindrome or not
        return n == reverse(n);
    }
}
