package Gr11;

/*Write a program in Java that accepts a number and displays
Pythagorean Triplets it is a part of. Eg: 3 4 --> 5*/

import java.util.Scanner;

public class Pythagorean_Triplet {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n;
        do { // Do-While loop that repeats until user enters a correct input
            System.out.print("Enter the number (n): ");
            n = in.nextInt();
            if (n < 1) {
                System.out.print("Invalid Input! Enter the values again\n\n");
            }
        } while (n < 1);
        Hypotenuse(n, true);
        System.out.println();
        Sides(n, true);
    }

    static int Hypotenuse(int n, boolean display) {
        int i, c;
        if (display) System.out.println("The Pythagorean Triplets in which " + n + " is the hypotenuse are: ");
        for (i = 2, c = 0; i < n; i += 2) {
            double odd = Math.sqrt((n * n) - (i * i));
            if (odd == (Math.round(odd))) {
                ++c;
                if (display) System.out.println((int) odd + "\t" + i + "\t-->\t" + n);
            }
        }
        if (c == 0 && display) {
            System.out.println("None!");
        }
        return c;
    }

    static int Sides(int n, boolean display) {
        int i, c;
        if (display) System.out.println("The Pythagorean Triplets in which " + n + " is the one of the sides are:");
        for (i = 1, c = 0; i <= 50000; i++) {
            if (Modulus(n, i) % 1 == 0 && i < (Modulus(n, i))) {
                ++c;
                if (display) System.out.println(n + "\t" + i + "\t-->\t" + (int) (Modulus(n, i)));
            }
        }
        if (c == 0 && display) {
            System.out.println("None!");
        }
        return c;
    }

    static double Modulus(int a, int b) {
        return Math.sqrt((a * a) + (b * b));
    }
}
