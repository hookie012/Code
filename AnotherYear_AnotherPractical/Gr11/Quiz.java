package Gr11;

/*The quiz has five questions with four multiple choices (A, B, C, D),
with each question carrying 1 mark for the correct answer. Design a
program to accept the number of participants N such that N must be
greater than 3 and less than 11. Create a double-dimensional array
of size (Nx5) to store the answers of each participant row-wise.
Calculate the marks for each participant by matching the correct
answer stored in a single-dimensional array of size 5. Display the
scores for each participant and also the participant(s) having the
highest score.*/

import java.util.Scanner;

public class Quiz {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n, i, j;
        char t;
        String aa;
        do { // Do-While loop that repeats until user enters a correct input
            System.out.print("Enter the number of participants (n): ");
            n = in.nextInt();
            if (n < 4 || n > 10) {
                System.out.print("Invalid Input! Enter the values again\n\n");
            }
        } while (n < 4 || n > 10);
        char[] key = new char[5];
        sda_input(key);
        int[] score = new int[n];
        for (i = 0; i < n; i++) {
            score[i] = 0;
        }
        System.out.println("Enter the answers of participants (as one word):");
        for (i = 0; i < n; i++) {
            do {
                System.out.print("Participant " + (i + 1) + ":\t");
                aa = in.next().toUpperCase();
                if (aa.length() != 5) {
                    System.out.println("\nInvalid Input! Enter the answers of the participant again:\nParticipant " + (i + 1) + ":\t");
                }
            } while (aa.length() != 5);
            for (j = 0; j < 5; j++) {
                t = aa.charAt(j);
                if (t == key[j]) {
                    score[i]++;
                }
            }
        }
        System.out.println("Scores: ");
        for (i = 0; i < n; i++) {
            System.out.println("Participant " + (i + 1) + ": " + score[i]);
        }
        System.out.println("Highest Score: ");
        for (i = 0; i < n; i++) {
            if (score[i] == sda_max(score)) {
                System.out.println("Participant " + (i + 1));
            }
        }
    }

    static void sda_input(char[] a) {
        Scanner in = new Scanner(System.in);
        System.out.println("Enter the answer key (as one word):");
        String aa = in.next().toUpperCase();
        for (int i = 0; i < 5; i++) {
            a[i] = aa.charAt(i);
        }
    }

    static int sda_max(int[] a) {
        int max = 0;
        for (int j : a) {
            max = Math.max(max, j);
        }
        return max;
    }
}
