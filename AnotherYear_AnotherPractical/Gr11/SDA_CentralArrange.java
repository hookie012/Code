package Gr11;

/*Write a program to accept a set of n integers (where n > 0) in a
single dimensional array. Arrange the elements of the array such that
the lowest number appears in the centre of the array, next lower
number in the right cell of the centre, next lower in the left cell of the
centre and so on... . The process will stop when the highest number
will set in its appropriate cell. Finally, display the array elements.*/

import java.util.Scanner;

public class SDA_CentralArrange {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n;
        do { // Do-While loop that repeats until user enters a correct input
            System.out.print("Enter the number of elements (n): ");
            n = in.nextInt();
            if (n <= 0) {
                System.out.print("Invalid Input! Enter the value again\n\n");
            }
        } while (n <= 0);
        int[] a = new int[n];
        sda_input(a, n - 1);
        sda_centralarrange(a);
        sda_display(a, n - 1);
    }

    static void sda_centralarrange(int[] a) {
        int t = 0;
        sda_insort(a, a.length - 1);
        if (a.length % 2 == 0) {
            t = sda_max(a, a.length - 1);
        }
        for (int i = a.length - 1, j = 1; i >= 0; i--) {
            if (i % 2 == 1) {
                sda_switch(a, i, a.length - (j++));
            }
        }
        sda_insort(a, (a.length - 1) / 2);
        sda_reverse(a, (a.length - 1) / 2);
        if (a.length % 2 == 0) {
            a[a.length - 1] = t;
        }
    }

    static void sda_insort(int[] a, int end) {
        for (int i = 1; i < (end + 1); i++) {
            for (int j = i; j > 0; j--) {
                if ((a[j] < a[j - 1])) {
                    sda_switch(a, j - 1, j);
                }
            }
        }
    }

    static void sda_switch(int[] a, int i, int j) {
        a[i] = a[i] + a[j];
        a[j] = a[i] - a[j];
        a[i] = a[i] - a[j];
    }

    static void sda_display(int[] a, int end) {
        end = Math.min(a.length - 1, end);
        for (int i = 0; i <= end; i++) {
            System.out.print(a[i] + ", ");
        }
        System.out.print("\b\b\n");
    }

    static void sda_input(int[] a, int end) {
        Scanner in = new Scanner(System.in);
        end = Math.min(a.length - 1, end);
        System.out.println("Enter the elements: ");
        for (int i = 0; i <= end; i++) {
            a[i] = in.nextInt();
        }
    }

    static void sda_reverse(int[] a, int end) {
        int[] b = new int[end + 1];
        for (int i = end, j = 0; i >= 0; i--, j++) {
            b[j] = a[i];
        }
        System.arraycopy(b, 0, a, 0, b.length);
    }

    static int sda_max(int[] a, int end) {
        int max = 0;
        for (int i = 0; i <= end; i++) {
            max = Math.max(max, a[i]);
        }
        return max;
    }
}
