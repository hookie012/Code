package Gr11;

/*Write a Program in Java to input elements in a 2-D square matrix
and check whether it is a Scalar Matrix or not.
Scalar Matrix: A scalar matrix is a diagonal matrix where the left
diagonal elements are same.*/

import java.util.Scanner;

public class Scalar_Matrix {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n, i;
        do { // Do-While loop that repeats until user enters a correct input
            System.out.print("Enter the order (n): ");
            n = in.nextInt();
            if (n < 1) {
                System.out.print("Invalid Input! Enter the values again\n\n");
            }
        } while (n < 1);
        int[][] a = new int[n][n];
        int[] diagonal = new int[n];
        dda_input(a, n, n);
        for (i = 0; i < n; i++) {
            diagonal[i] = a[i][i];
        }
        dda_display(a, n, n);
        if (n == sda_frequency(diagonal, diagonal[0], diagonal.length - 1)) {
            System.out.println("It is a Scalar Matrix");
            System.exit(0);
        }
        System.out.println("It is not a Scalar Matrix");
    }

    static void dda_display(int[][] a, int r, int c) {
        System.out.print("");
        for (int i = 0; i < r; i++) {
            for (int j = 0; j < c; j++) {
                System.out.print(a[i][j] + "\t");
            }
            System.out.println();
        }
    }

    static void dda_input(int[][] a, int r, int c) {
        Scanner in = new Scanner(System.in);
        System.out.print("Enter the elements: \n");
        for (int i = 0; i < r; i++) {
            for (int j = 0; j < c; j++) {
                a[i][j] = in.nextInt();
            }
        }
    }

    static int sda_frequency(int[] a, int element, int end) {
        int i, c;
        System.out.print("");
        for (i = 0, c = 1; i <= end - 1; i++) {
            if (a[i] == a[i + 1] && a[i] == element) {
                ++c;
            }
        }
        return c;
    }
}
