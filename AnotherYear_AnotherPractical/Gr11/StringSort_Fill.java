package Gr11;

/*Write a program in Java to accept a string. Arrange all the letters of
the string in alphabetical order. Now, insert the missing letters in
the sorted string to complete all the letters between first and last
characters of the string.*/

import java.util.Scanner;

public class StringSort_Fill {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Enter a string: ");
        stringsort_fill(in.nextLine().toLowerCase());
    }

    static void stringsort_fill(String s) {
        char lcL = 255, lcH = 0;
        for (int i = 0; i < s.length(); i++) {
            char ch = s.charAt(i);
            if (Character.isLowerCase(ch)) {
                if (ch < lcL) {
                    lcL = ch;
                }
                if (ch > lcH) {
                    lcH = ch;
                }
            }
        }
        System.out.print("Ordered String:\t");
        String st = "";
        for (char ch = 'a'; ch <= 'z'; ch++) {
            for (int i = 0; i < s.length(); i++) {
                if (ch == s.charAt(i)) {
                    st += ch;
                }
            }
        }
        System.out.print(st + "\nFilled String: \t");
        for (int i = lcL; i <= lcH; i++) {
            System.out.print((char) (i));
        }
    }
}
