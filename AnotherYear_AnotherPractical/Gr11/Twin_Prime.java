package Gr11;

/*Write a program in Java to generate all the twin primes in the range 1 to N, where
the value of N is entered by the user.*/

import java.util.Scanner;

public class Twin_Prime {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n, c, i;
        do { // Do-While loop that repeats until user enters a correct input
            System.out.print("Enter the upper limit (n): ");
            n = in.nextInt();
            if (1 >= n) {
                System.out.print("Invalid Input! Enter the values again\n\n");
            }
        } while (1 >= n);
        System.out.println("The Twin Prime Numbers are: ");
        for (i = 1, c = 0; i <= n; i++) {
            if (isPrime(i) && isPrime(i + 2)) {
                ++c;
                System.out.println(i + ", " + (i + 2));
            }
        }
        if (c == 0) System.out.println("None!");
    }

    static boolean isPrime(int n) { //Function to check whether n is a prime number or not
        int c = 0;
        if (n == 1 || n == 4) {
            return false;
        } else if (n == 2) {
            return true;
        } else {
            for (int i = 2; i < (n / 2); i++) {
                if (n % i == 0) {
                    ++c;
                }
            }
        }
        return c == 0;
    }
}
