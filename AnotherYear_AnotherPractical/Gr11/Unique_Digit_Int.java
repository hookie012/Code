package Gr11;

/*A unique digit Integer Is a positive integer without leading zeros.
Given 2 positive integers m and n_ Assume m is less than 30000 and n<30000. You are to output the
number of unique-digit integers in the specified range along with their values in the format specified
below:*/

import java.util.Scanner;

public class Unique_Digit_Int {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int m, n, c, i;
        do { // Do-While loop that repeats until user enters a correct input
            System.out.print("Enter the lower limit (m): ");
            m = in.nextInt();
            System.out.print("Enter the upper limit (n): ");
            n = in.nextInt();
            if (m >= n || m < 1 || n >= 30000) {
                System.out.print("Invalid Input! Enter the values again\n\n");
            }
        } while (m >= n || m < 1 || n >= 30000);
        System.out.println("The Unique Digit Numbers are: ");
        for (i = m, c = 0; i <= n; i++) {
            if (has_only_UniqueDigits(i)) {
                ++c;
                System.out.print(i + ", ");
            }
        }
        if (c == 0) System.out.print("None!  ");
        System.out.println("\b\b\nFrequency: " + c); //To remove the extra commas (,) at the end and display the frequency
    }

    static boolean has_only_UniqueDigits(int n) {
        Unique_Digit_Int Array = new Unique_Digit_Int();
        int[] arr = new int[no_digits(n)];
        for (int i = 0; i < arr.length; n /= 10, i++) {
            arr[i] = n % 10;
        }
        Array.insort(arr);
        for (int i = 0; i < arr.length - 1; i++) {
            if (arr[i] == arr[i + 1]) {
                return false;
            }
        }
        return true;
    }

    static int no_digits(int n) {
        n = Math.abs(n);
        for (int i = 1; ; i++) {
            if (n < (int) (Math.pow(10, i))) {
                return i;
            }
        }
    }

    void insort(int[] a) {
        for (int i = 1; i < a.length; i++) {
            for (int j = i; j > 0; j--) {
                if ((a[j] < a[j - 1])) {
                    a[j] = a[j] + a[j - 1];
                    a[j - 1] = a[j] - a[j - 1];
                    a[j] = a[j] - a[j - 1];
                }
            }
        }
    }
}