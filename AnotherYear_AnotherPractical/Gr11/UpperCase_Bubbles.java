package Gr11;

/*Write a program in Java to enter a string in a mixed case. Arrange all
the letters of string such that all the lower case characters are
followed by the upper case characters.*/

import java.util.Scanner;

public class UpperCase_Bubbles {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Enter the string: ");
        String s = in.nextLine();
        StringBuilder lc = new StringBuilder();
        StringBuilder uc = new StringBuilder();
        for (int i = 0; i < s.length(); i++) {
            char ch = s.charAt(i);
            if (Character.isLowerCase(ch))
                lc.append(ch);
            else if (Character.isUpperCase(ch))
                uc.append(ch);
        }
        System.out.println("Input String:\t" + s + "\nNew String: \t" + lc.append(uc));
    }
}
