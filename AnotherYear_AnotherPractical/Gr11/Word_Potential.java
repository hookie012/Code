package Gr11;

/*The encryption of letters are to be done as follows:A = 1 B = 2 C = 3 . . .Z = 26
The potential of a word is found by adding the encrypted value of the
letters.
Example: KITE
Potential = 11 + 9 + 20 + 5 = 45
Accept a sentence which is terminated by either " . " , " ? " or " ! ".
Each word of sentence is separated by single space. Decode the
words according to their potential and arrange them
in alphabetical order increasing order of their potential.*/

import java.util.Scanner;
import java.util.StringTokenizer;

public class Word_Potential {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String s, t;
        do { // Do-While loop that repeats until user enters a correct input
            System.out.print("Enter the sentence: ");
            s = in.nextLine().toUpperCase();
            if (s.charAt(s.length() - 1) != '.' && s.charAt(s.length() - 1) != ',' && s.charAt(s.length() - 1) != '!' && s.charAt(s.length() - 1) != '?') {
                System.out.print("Invalid Input! Enter the values again\n\n");
            }
        } while (s.charAt(s.length() - 1) != '.' && s.charAt(s.length() - 1) != ',' && s.charAt(s.length() - 1) != '!' && s.charAt(s.length() - 1) != '?');
        StringTokenizer str = new StringTokenizer(s.substring(0, s.length() - 1));
        int no_w = str.countTokens();
        String[] a = new String[no_w];
        System.out.println("Potential: ");
        for (int i = 0; i < no_w; i++) {
            t = str.nextToken();
            System.out.println(t + "\t=\t" + word_potential(t));
            a[i] = t;
        }
        for (int i = 1; i < a.length; i++) {
            for (int j = i; j > 0; j--) {
                if (word_potential(a[j]) < word_potential(a[j - 1])) {
                    sda_switch(a, j - 1, j);
                }
            }
        }
        sda_display(a, a.length - 1);
    }

    static int word_potential(String word) {
        int S = 0;
        if (word.length() == 1) {
            return ((int) (word.charAt(0))) - 64;
        } else {
            S = S + ((int) (word.charAt(0))) + word_potential(word.substring(1)) - 64;
        }
        return S;
    }

    static void sda_switch(String[] a, int i, int j) {
        String t = a[j];
        a[j] = a[i];
        a[i] = t;
    }

    static void sda_display(String[] a, int end) {
        end = Math.min(a.length - 1, end);
        for (int i = 0; i <= end; i++) {
            System.out.print(a[i] + " ");
        }
        System.out.print("\b\n");
    }
}
