package Gr12;

import java.util.Arrays;
import java.util.Scanner;

public class Number_Fascinating {
    public static void main(String[] args) {
        check_isFascinating(input("Enter the number: "));
    }

    static long input(String display) {
        Scanner in = new Scanner(System.in);
        long n;
        do { // Do-While loop that repeats until user enters a correct input
            System.out.print(display);
            n = in.nextLong();
            if (n < 1)
                System.out.print("Invalid Input! Enter the value(s) again!\n\n");
        } while (n < 1);
        return n;
    }

    static void check_isFascinating(long n) {
        if (n < 100) {
            System.out.println(n + " is not a Fascinating Number!");
            System.exit(0);
        }
        String n1 = Integer.toString((int) n);
        String n2 = Integer.toString((int) (2 * n));
        String n3 = Integer.toString((int) (3 * n));
        n = Long.parseLong(n1 + n2 + n3);
        System.out.println(n);
        if (isFascinating(n))
            System.out.println(n + " is a Fascinating Number!");
        else
            System.out.println(n + " is not a Fascinating Number!");
    }

    static boolean isFascinating(long n) {
        long[] a = new long[no_digits(n)];
        for (int i = 0; i < a.length; n /= 10, i++)
            a[i] = n % 10;
        insort(a);
        int start = 0;
        for (int i = 0; i < a.length; i++)
            if (a[i] != 0) {
                start = i;
                break;
            }
        long[] x = new long[a.length - start];
        System.arraycopy(a, start, x, 0, a.length - start);
        long[] b = {1, 2, 3, 4, 5, 6, 7, 8, 9};
        return Arrays.equals(x, b);
    }

    static int no_digits(long n) {
        n = Math.abs(n);
        for (int i = 1; ; i++)
            if (n < (Math.pow(10, i)))
                return i;
    }

    static void insort(long[] a) {
        for (int i = 1; i < a.length; i++)
            for (int j = i; j > 0; j--)
                if ((a[j] < a[j - 1]))
                    swap(a, j, j - 1);
    }

    static void swap(long[] a, int i, int j) {
        a[i] = a[i] + a[j];
        a[j] = a[i] - a[j];
        a[i] = a[i] - a[j];
    }
}