package Gr12;

import java.util.Scanner;

public class Number_Goldbach {
    public static void main(String[] args) {
        check_isGoldbach(input("Enter the number: "));
    }

    static int input(String display) {
        Scanner in = new Scanner(System.in);
        int n;
        do { // Do-While loop that repeats until user enters a correct input
            System.out.print(display);
            n = in.nextInt();
            if (n < 1)
                System.out.print("Invalid Input! Enter the value(s) again!\n\n");
        } while (n < 1);
        return n;
    }


    static void check_isGoldbach(int n) {
        if (n % 2 == 0)
            System.out.println(n + " is a Goldbach Number.\nThe Pairs of Primes are: ");
        else {
            System.out.println((n + " is not a Goldbach Number."));
            System.exit(0);
        }
        int c = 0;
        for (int i = 0, m = n / 2; i < m; i++)
            if (isPrime(m - i) && isPrime(m + i) && c++ == c - 1)
                System.out.println((m - i) + "\t" + (m + i));
        System.out.println("The number of pairs: " + c);
    }

    static boolean isPrime(int n) {
        if (n < 2)
            return false;
        else if (n < 4)
            return true;
        else if (n % 2 == 0 || n % 3 == 0)
            return false;
        for (int i = 5; i * i < n + 1; i += 6)
            if (n % i == 0 || n % (i + 2) == 0)
                return false;
        return true;
    }
}
