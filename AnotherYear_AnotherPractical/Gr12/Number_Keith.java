package Gr12;

import java.util.Scanner;

public class Number_Keith {
    public static void main(String[] args) {
        check_isKeith(input("Enter the number: "));
    }

    static int input(String display) {
        Scanner in = new Scanner(System.in);
        int n;
        do { // Do-While loop that repeats until user enters a correct input
            System.out.print(display);
            n = in.nextInt();
            if (n < 1)
                System.out.print("Invalid Input! Enter the value(s) again!\n\n");
        } while (n < 1);
        return n;
    }


    static void check_isKeith(int n) {
        System.out.println(isKeith(n));
    }

    static boolean isKeith(int n) {
        int m = reverse(n), no = no_digits(n), S = 0;
        int[] a = new int[no];
        for (int i = 0; i < a.length; i++, m /= 10) a[i] = m % 10;
        for (int k = 0; S < n + 1; k++) {
            S = sum(a);
            if (S == n)
                return true;
            a[k] = S;
            if (k == no - 1)
                k = -1;
        }
        return false;
    }

    public static int sum(int[] a) {
        int S = 0;
        for (int i : a)
            S += i;
        return S;
    }

    public static int no_digits(int n) {
        n = Math.abs(n);
        for (int i = 1; ; i++)
            if (n < (int) (Math.pow(10, i)))
                return i;
    }

    public static int reverse(int n) { // Function to reverse n and return the reversed number
        int reverse = 0;
        for (; n > 0; n /= 10)
            reverse = reverse * 10 + (n % 10);
        return reverse;
    }
}
