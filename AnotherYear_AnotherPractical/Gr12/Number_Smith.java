package Gr12;

/*
A Smith Number is a composite number whose sum of digits is equal to the sum of digits in its prime factorization.
*/

import java.util.Scanner;

public class Number_Smith {
    public static void main(String[] args) {
        check_isSmith(input("Enter the number: "));
    }

    static int input(String display) {
        Scanner in = new Scanner(System.in);
        int n;
        do { // Do-While loop that repeats until user enters a correct input
            System.out.print(display);
            n = in.nextInt();
            if (n < 1)
                System.out.print("Invalid Input! Enter the value(s) again!\n\n");
        } while (n < 1);
        return n;
    }

    static void check_isSmith(int n) {
        System.out.println("Sum of Digits it is prime factorization: " + sumofsumoffactors(n));
        if (isSmith(n)) {
            if (isPrime(n))
                System.out.println("But " + n + " is a Prime Number! \n" + n + " is not a Smith Number");
            else
                System.out.println(n + " is a Smith Number");
        } else {
            if (isPrime(n))
                System.out.println(n + " is a Prime Number! \n" + n + " is not a Smith Number");
            else
                System.out.println(n + " is not a Smith Number!");
        }
    }

    static boolean isSmith(int n) {
        return sumofdigits(n) == sumofsumoffactors(n);
    }

    static int sumofsumoffactors(int n) {
        int S = 0;
        for (int i = 2; i < n + 1; )
            if (n % i == 0) {
                S += sumofdigits(i);
                n /= i;
            } else
                i++;
        return S;
    }

    static boolean isPrime(int n) {
        if (n < 2)
            return false;
        else if (n < 4)
            return true;
        else if (n % 2 == 0 || n % 3 == 0)
            return false;
        for (int i = 5; i * i < n + 1; i += 6)
            if (n % i == 0 || n % (i + 2) == 0)
                return false;
        return true;
    }

    static int sumofdigits(int n) {
        int sumofdigits = 0;
        for (; n > 0; n /= 10)
            sumofdigits += (n % 10);
        return sumofdigits;
    }
}