package Gr12;

/*The number which is Prime Number, as well as Adam Number, is called
Prime Adam Number in Java. The number which will satisfy the below two
conditions will be called as Prime Adam Number. The given number should
be a prime number. And its square and the square of its reverse should
be reverse of each other.*/

import java.util.Scanner;

public class Numbers_Prime_Adam {
    public static void main(String[] args) {
        int m, n;
        do {
            m = input("Enter the lower limit (m): ");
            n = input("Enter the upper limit (n): ");
            if (m >= n)
                System.out.print("Invalid Input! Enter the value(s) again!\n\n");
        } while (m >= n);
        print_PrimeAdam(m, n);
    }

    static int input(String display) {
        Scanner in = new Scanner(System.in);
        int n;
        do { // Do-While loop that repeats until user enters a correct input
            System.out.print(display);
            n = in.nextInt();
            if (n < 1)
                System.out.print("Invalid Input! Enter the value(s) again!\n\n");
        } while (n < 1);
        return n;
    }

    static void print_PrimeAdam(int lower, int upper) {
        System.out.println("The Prime Adam Numbers are: ");
        int i, c;
        for (i = lower, c = 0; i < upper + 1; i++)
            if (isPrime(i) && isAdam(i))
                if (++c == 1)
                    System.out.print(i);
                else
                    System.out.print(", " + i);
        if (c == 0)
            System.out.println("None!  ");
        else
            System.out.println("\nFrequency: " + c);
    }

    static boolean isPrime(int n) {
        if (n < 2)
            return false;
        else if (n < 4)
            return true;
        else if (n % 2 == 0 || n % 3 == 0)
            return false;
        for (int i = 5; i * i < n + 1; i += 6)
            if (n % i == 0 || n % (i + 2) == 0)
                return false;
        return true;
    }

    static int reverse(int n) { // Function to reverse n and return the reversed number
        int reverse = 0;
        for (; n > 0; n /= 10)
            reverse = reverse * 10 + (n % 10);
        return reverse;
    }

    static boolean isAdam(int n) {
        return ((n * n) == reverse((int) Math.pow(reverse(n), 2)));
    }
}
