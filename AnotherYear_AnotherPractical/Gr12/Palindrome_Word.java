package Gr12;

import java.util.Scanner;
import java.util.StringTokenizer;

public class Palindrome_Word {
    public static void main(String[] args) {
        palindromize(input("Enter the sentence: "));
    }

    static String input(String display) {
        Scanner in = new Scanner(System.in);
        String n;
        do { // Do-While loop that repeats until user enters a correct input
            System.out.print(display);
            n = in.nextLine();
            if (".?!".indexOf(n.charAt(n.length() - 1)) == -1)
                System.out.print("Invalid Input! Enter the value(s) again!\n\n");
        } while (".?!".indexOf(n.charAt(n.length() - 1)) == -1);
        return n;
    }

    static void palindromize(String n) {
        char l = n.charAt(n.length() - 1);
        n = n.substring(0, n.length() - 1);
        StringTokenizer st = new StringTokenizer(n);
        StringBuilder s = new StringBuilder();
        int c = 0;
        while (st.hasMoreTokens()) {
            String w = st.nextToken();
            String substring = (w + " ").substring(0, w.length());
            if (++c == 1)
                s.append(palindromise(substring));
            else
                s.append(" ").append(palindromise(substring));
        }
        System.out.println(s + "" + l);
    }


    static String palindromise(String n) {
        if (isPalindrome(n))
            return n;
        else
            return n + reverse(n.substring(0, n.length() - 1));
    }

    static boolean isPalindrome(String n) {
        return (n.equals(reverse(n)));
    }

    static String reverse(String n) {
        StringBuilder s = new StringBuilder();
        for (int i = 0; i < n.length(); i++)
            s.append(n.charAt(n.length() - 1 - i));
        return String.valueOf(s);
    }
}
