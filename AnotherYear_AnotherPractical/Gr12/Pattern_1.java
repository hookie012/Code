package Gr12;

import java.util.Scanner;

public class Pattern_1 {
    public static void main(String[] args) {
        print_pattern(input("Enter the number of rows and columns: "), "");
    }

    static int input(String display) {
        Scanner in = new Scanner(System.in);
        int n;
        do { // Do-While loop that repeats until user enters a correct input
            System.out.print(display);
            n = in.nextInt();
            if (n < 1)
                System.out.print("Invalid Input! Enter the value(s) again!\n\n");
        } while (n < 1);
        return n;
    }

    static void print_pattern(int n, String display) {
        String[][] a = new String[n][n];
        for (int i = 0; i < n; i++)
            for (int j = 0; j < n; j++)
                a[i][j] = Integer.toString(1 + j);
        int k = (int) Math.ceil((double) n / 2) - 1;
        int e = (n % 2 == 0) ? 1 : 0;
        for (int i = 0; i < n; i++)
            for (int j = 0; j < n; j++)
                if (i <= k) {
                    if (j < (k + e + i) && j > (k - i))
                        a[i][j] = " ";
                } else {
                    if ((j < (k + e + (n - (i + 1)))) && j > k - (n - (i + 1)))
                        a[i][j] = " ";
                }
        dda_display(a, n, n, display, ((n > 10) ? "\t" : " "));
    }

    static void dda_display(String[][] a, int r, int c, String display, String separator) {
        System.out.print(display);
        for (int i = 0; i < r; i++) {
            for (int j = 0; j < c; j++)
                System.out.print(a[i][j] + "" + separator);
            System.out.println();
        }
    }
}
