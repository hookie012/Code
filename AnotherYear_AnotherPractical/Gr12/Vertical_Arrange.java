package Gr12;

import java.util.Scanner;

public class Vertical_Arrange {
    public static void main(String[] args) {
        v_display(input("Enter the number of words: "));
    }

    static int input(String display) {
        Scanner in = new Scanner(System.in);
        int n;
        do { // Do-While loop that repeats until user enters a correct input
            System.out.print(display);
            n = in.nextInt();
            if (n < 1)
                System.out.print("Invalid Input! Enter the value(s) again!\n\n");
        } while (n < 1);
        return n;
    }

    static void v_display(int n) {
        Scanner in = new Scanner(System.in);
        String[] word = new String[n];
        int[] length = new int[n];
        int l = 0;
        System.out.println("Enter the words");
        for (int i = 0; i < n; i++) {
            word[i] = in.next();
            length[i] = word[i].length();
            l = Math.max(l, length[i]);
        }
        char[][] arr = new char[l][n];
        for (int i = 0; i < l; i++)
            for (int j = 0; j < n; j++)
                if (!(i > length[j] - 1))
                    arr[i][j] = word[j].charAt(i);
                else
                    arr[i][j] = ' ';
        dda_display(arr, l, n, "\nThe names arranged as in a vertical banner:\n");
    }

    static void dda_display(char[][] a, int r, int c, String display) {
        System.out.print(display);
        for (int i = 0; i < r; i++) {
            for (int j = 0; j < c; j++)
                System.out.print(a[i][j] + " ");
            System.out.println();
        }
    }
}
