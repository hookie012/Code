package Methods;

import java.util.stream.IntStream;

public class Maeth {
    public static void main(String[] args) {

    }

    static int aliquot_sum(int n) {
        int S = 0;
        for (int i = 1; i <= (n / 2); i++) {
            S += (n % i == 0) ? i : 0;
        }
        return S;
    }

    static int sumofdigit_cubed(int n) {
        int sumofcubes = 0;
        for (; n > 0; n /= 10) {
            sumofcubes += (int) (Math.pow(n % 10, 3));
        }
        return sumofcubes;
    }

    static int reverse(int n) { // Function to reverse n and return the reversed number
        int reverse = 0;
        for (; n > 0; n /= 10) {
            reverse = reverse * 10 + (n % 10);
        }
        return reverse;
    }

    static boolean isPrime(int n) {
        if (n <= 1 || n % 2 == 0 || n % 3 == 0) {
            return false;
        }
        for (int i = 5; i * i <= n; i = i + 6) {
            if (n % i == 0 || n % (i + 2) == 0) {
                return false;
            }
        }
        return true;
    }

    static void print_Prime(int lower, int upper) {
        boolean[] prime = new boolean[upper + 1];
        for (int i = 0; i <= upper; i++) {
            prime[i] = true;
        }
        for (int i = 2; i * i <= upper; i++) {
            if (prime[i]) {
                for (int j = i * i; j <= upper; j += i) {
                    prime[j] = false;
                }
            }
        }
        for (int i = lower; i <= upper; i++) {
            if (prime[i])
                System.out.print(i + " ");
        }
    }

    static boolean isComposite(int n) { //Function to check whether n is a prime number or not
        return ((int) IntStream.range(1, n + 1).filter(i -> n % i == 0).count()) != 2;
    }

    static boolean isMagic(int n) { //Recursive Method to check whether n is a Magic Number or not
        if (n == 1) {
            return true;
        } else if (no_digits(n) == 1) {
            return false;
        } else {
            return isMagic(sumofdigits(n));
        }
    }

    static int sumofdigits(int n) {
        int sumofdigits = 0;
        for (; n > 0; n /= 10) {
            sumofdigits += (n % 10);
        }
        return sumofdigits;
    }

    static int sumoffactors(int n) {
        int S = 0;
        for (int i = 2; i < n + 1; i++) {
            if (n % i == 0) {
                S += i;
                n /= i;
            }
        }
        return S;
    }

    static int sumofsumoffactors(int n) {
        int S = 0;
        for (int i = 2; i < n + 1; i++) {
            if (n % i == 0) {
                S += sumofdigits(i);
                n /= i;
            }
        }
        return S;
    }

    static int no_digits(int n) {
        n = Math.abs(n);
        for (int i = 1; ; i++) {
            if (n < (int) (Math.pow(10, i))) {
                return i;
            }
        }
    }

    static int no_digits(int n, int base) {
        n = Math.abs(n);
        for (int i = 1; ; i++) {
            if (n < (int) (Math.pow(base, i))) {
                return i;
            }
        }
    }

    static String tobase(String number, int obase, int nbase) {
        return Integer.toString(Integer.parseInt(number, obase), nbase);
    }
}
