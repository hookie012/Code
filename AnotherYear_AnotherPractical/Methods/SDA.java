package Methods;

import java.util.Scanner;

public class SDA {
    public static void main(String[] args) {

    }

    static int search(int[] a, int start, int end, int n) {
        int mid = (start + end) / 2;
        sort(a);
        if (start > end) {
            return -1;
        } else {
            if (n == a[mid]) {
                return mid;
            } else if (n < a[mid]) {
                return search(a, start, mid - 1, n);
            } else if (n > mid) {
                return search(a, mid + 1, end, n);
            }
            return Integer.MIN_VALUE;
        }
    }

    static void sort(int[] a) {
        for (int i = 1; i < a.length; i++) {
            for (int j = i; j > 0; j--) {
                if (a[j] < a[j - 1]) {
                    swap(a, j - 1, j);
                }
            }
        }
    }

    static void sort(int[] a, String descending) {
        for (int i = 1; i < a.length; i++) {
            for (int j = i; j > 0; j--) {
                if (a[j] > a[j - 1]) {
                    swap(a, j - 1, j);
                }
            }
        }
    }

    static void sort(int[] a, int end) {
        end = Math.abs(end);
        for (int i = 1; i < (end + 1); i++) {
            for (int j = i; j > 0; j--) {
                if (a[j] < a[j - 1]) {
                    swap(a, j - 1, j);
                }
            }
        }
    }

    static void sort(int[] a, int end, String descending) {
        end = Math.abs(end);
        for (int i = 1; i < (end + 1); i++) {
            for (int j = i; j > 0; j--) {
                if (a[j] > a[j - 1]) {
                    swap(a, j - 1, j);
                }
            }
        }
    }

    static void sort(int[] a, int start, int end) {
        end = Math.abs(end);
        for (int i = start + 1; i < (end + 1); i++) {
            for (int j = i; j > 0; j--) {
                if (a[j] < a[j - 1]) {
                    swap(a, j - 1, j);
                }
            }
        }
    }

    static void sort(int[] a, int start, int end, String descending) {
        end = Math.abs(end);
        for (int i = start + 1; i < (end + 1); i++) {
            for (int j = i; j > 0; j--) {
                if (a[j] > a[j - 1]) {
                    swap(a, j - 1, j);
                }
            }
        }
    }

    static void sort(int[] a, int start, int end, boolean ascending) {
        end = Math.abs(end);
        for (int i = start + 1; i < (end + 1); i++) {
            for (int j = i; j > 0; j--) {
                if ((ascending) ? (a[j] < a[j - 1]) : (a[j] > a[j - 1])) {
                    swap(a, j - 1, j);
                }
            }
        }
    }

    static void swap(int[] a, int i, int j) {
        a[i] = a[i] + a[j];
        a[j] = a[i] - a[j];
        a[i] = a[i] - a[j];
    }

    static void display(int[] a, int start, int end) {
        if (end < start) {
            start = start + end;
            end = start - end;
            start = start - end;
        }
        end = Math.min(a.length - 1, end);
        for (int i = start; i <= end; i++) {
            System.out.print(a[i] + ", ");
        }
        System.out.print("\b\b\n");
    }

    static void input(int[] a, String display) {
        Scanner in = new Scanner(System.in);
        System.out.print(display);
        for (int i = 0; i < a.length; i++) {
            a[i] = in.nextInt();
        }
    }

    static void input(int[] a, int end, String display) {
        end = Math.abs(end);
        Scanner in = new Scanner(System.in);
        System.out.print(display);
        end = Math.min(a.length - 1, end);
        for (int i = 0; i < end + 1; i++) {
            a[i] = in.nextInt();
        }
    }

    static void input(int[] a, int start, int end, String display) {
        end = Math.abs(end);
        if (end < start) {
            start = start + end;
            end = start - end;
            start = start - end;
        }
        Scanner in = new Scanner(System.in);
        System.out.print(display);
        end = Math.min(a.length - 1, end);
        for (int i = start; i < end + 1; i++) {
            a[i] = in.nextInt();
        }
    }

    static void input(char[] a, String display) {
        Scanner in = new Scanner(System.in);
        System.out.print(display);
        for (int i = 0; i < a.length; i++) {
            a[i] = in.next().charAt(0);
        }
    }

    static void input(char[] a, int end, String display) {
        Scanner in = new Scanner(System.in);
        System.out.print(display);
        end = Math.min(a.length - 1, end);
        for (int i = 0; i < end + 1; i++) {
            a[i] = in.next().charAt(0);
        }
    }

    static void input(char[] a, int start, int end, String display) {
        Scanner in = new Scanner(System.in);
        System.out.print(display);
        end = Math.min(a.length - 1, end);
        for (int i = start; i < end + 1; i++) {
            a[i] = in.next().charAt(0);
        }
    }

    static void reverse(int[] a) {
        int[] b = new int[a.length];
        for (int i = a.length - 1, j = 0; i >= 0; i--, j++) {
            b[j] = a[i];
        }
        System.arraycopy(b, 0, a, 0, b.length);
    }

    static void reverse(int[] a, int end) {
        int[] b = new int[end + 1];
        for (int i = end, j = 0; i >= 0; i--, j++) {
            b[j] = a[i];
        }
        System.arraycopy(b, 0, a, 0, end + 1);
    }

    static void reverse(int[] a, int start, int end) {
        int[] b = new int[end - start + 1];
        for (int i = end, j = 0; i >= start; i--, j++) {
            b[j] = a[i];
        }
        for (int i = start, j = 0; i < end + 1; i++, j++) {
            a[i] = b[j];
        }
    }

    static int max(int[] a) {
        int max = a[0];
        for (int i = 1; i < a.length; i++) {
            max = Math.max(max, a[i]);
        }
        return max;
    }

    static int max(int[] a, int end) {
        int max = a[0];
        for (int i = 1; i < end + 1; i++) {
            max = Math.max(max, a[i]);
        }
        return max;
    }

    static int max(int[] a, int start, int end) {
        int max = a[start];
        for (int i = start + 1; i < end + 1; i++) {
            max = Math.max(max, a[i]);
        }
        return max;
    }

    static int min(int[] a) {
        int min = a[0];
        for (int i = 1; i < a.length; i++) {
            min = Math.min(min, a[i]);
        }
        return min;
    }

    static int min(int[] a, int end) {
        int min = a[0];
        for (int i = 1; i < end + 1; i++) {
            min = Math.min(min, a[i]);
        }
        return min;
    }

    static int min(int[] a, int start, int end) {
        int min = a[start];
        for (int i = start + 1; i < end + 1; i++) {
            min = Math.min(min, a[i]);
        }
        return min;
    }

    static void frequency(int[] a, int start, int end, String display) {
        sort(a, 0, a.length - 1);
        int i, c;
        System.out.print(display);
        for (i = start, c = 1; i < end; i++) {
            if (a[i] == a[i + 1]) {
                ++c;
            } else {
                System.out.println(a[i] + "\t\t\t" + c);
                c = 1;
            }
        }
        System.out.println(a[i] + "\t\t\t" + c);
    }
}
