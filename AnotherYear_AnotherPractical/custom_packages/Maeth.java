package custom_packages;

import java.util.concurrent.ThreadLocalRandom;

public class Maeth {
    public static void main(String[] args) {

    }

    public static int random(int origin_in, int bound_in) {
        return ThreadLocalRandom.current().nextInt(origin_in, bound_in + 1);
    }

    public static int aliquot_sum(int n) {
        int S = 0;
        for (int i = 1; i <= (n / 2); i++) {
            S += (n % i == 0) ? i : 0;
        }
        return S;
    }

    public static int sumofdigit_cubed(int n) {
        int sumofcubes = 0;
        for (; n > 0; n /= 10) {
            sumofcubes += (int) (Math.pow(n % 10, 3));
        }
        return sumofcubes;
    }

    public static int reverse(int n) { // Function to reverse n and return the reversed number
        int reverse = 0;
        for (; n > 0; n /= 10) {
            reverse = reverse * 10 + (n % 10);
        }
        return reverse;
    }

    public static boolean isPrime(int n) {
        if (n < 2) {
            return false;
        } else if (n < 4) {
            return true;
        } else if (n % 2 == 0 || n % 3 == 0) {
            return false;
        }
        for (int i = 5; i * i < n + 1; i += 6) {
            if (n % i == 0 || n % (i + 2) == 0) {
                return false;
            }
        }
        return true;
    }

    public static void print_Primes(int lower, int upper) {
        boolean[] prime = new boolean[upper + 1];
        Arrey.fill(prime, true);
        for (int i = 2; i * i < upper + 1; i++) {
            if (prime[i]) {
                for (int j = i * i; j < upper + 1; j += i) {
                    prime[j] = false;
                }
            }
        }
        for (int i = lower; i < upper + 1; i++) {
            if (prime[i]) {
                System.out.print(i + " ");
            }
        }
    }

    public static boolean isComposite(int n) { //Function to check whether n is a prime number or not
        return n >= 2 && !isPrime(n);
    }

    public static boolean isMagic(int n) { //Recursive Method to check whether n is a Magic Number or not
        if (n == 1) {
            return true;
        } else if (n < 10) {
            return false;
        } else {
            return isMagic(sumofdigits(n));
        }
    }

    public static int sumofdigits(int n) {
        int sumofdigits = 0;
        for (; n > 0; n /= 10) {
            sumofdigits += (n % 10);
        }
        return sumofdigits;
    }

    public static int sumoffactors(int n) {
        int S = 0;
        for (int i = 2; i < n + 1; i++) {
            if (n % i == 0) {
                S += i;
                n /= i;
            }
        }
        return S;
    }

    public static int sumofsumoffactors(int n) {
        int S = 0;
        for (int i = 2; i < n + 1; i++) {
            if (n % i == 0) {
                S += sumofdigits(i);
                n /= i;
            }
        }
        return S;
    }

    public static int no_digits(int n) {
        n = Math.abs(n);
        for (int i = 1; ; i++) {
            if (n < (int) (Math.pow(10, i))) {
                return i;
            }
        }
    }

    public static int no_digits(int n, int base) {
        n = Math.abs(n);
        for (int i = 1; ; i++) {
            if (n < (int) (Math.pow(base, i))) {
                return i;
            }
        }
    }

    public static String tobase(String number, int obase, int nbase) {
        return Integer.toString(Integer.parseInt(number, obase), nbase);
    }
}
