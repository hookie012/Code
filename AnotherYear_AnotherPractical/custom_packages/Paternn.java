package custom_packages;

public class Paternn {
    public static void print_pattern(int n, int type) {
        if (type == 1) {
            String[][] a = new String[n][n];
            for (int i = 0; i < n; i++) {
                for (int j = 0; j < n; j++) {
                    a[i][j] = Integer.toString(1 + j);
                }
            }
            int k = (int) Math.ceil((double) n / 2) - 1;
            int e = (n % 2 == 0) ? 1 : 0;
            for (int i = 0; i < n; i++) {
                for (int j = 0; j < n; j++) {
                    if (i <= k) {
                        if (j < (k + e + i) && j > (k - i)) {
                            a[i][j] = " ";
                        }
                    } else {
                        if (j < (k + e + (n - (i + 1))) && j > (k - (n - (i + 1)))) {
                            a[i][j] = " ";
                        }
                    }
                }
            }
            Arrey.print(a, n, n, "", ((n > 10) ? "\t" : " "));
        }
    }
}
