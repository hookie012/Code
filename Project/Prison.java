import custom_packages.Arrey;
import custom_packages.Maeth;

import java.util.Scanner;

public class Prison {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt(), r = 0;
        int[] a = new int[n], b = new int[n];
        for (int i = 0; i < n; i++) {
            a[i] = i + 1;
            do {
                r = Maeth.random(1, n);
            } while (Arrey.l_search(b, r) != -1);
            b[i] = r;
        }
        Arrey.print(b);
    }
}
