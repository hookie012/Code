import java.util.Scanner;
import java.util.StringTokenizer;

public class ParaSplit {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String s;
        do {
            System.out.print("Enter the sentence: ");
            s = in.nextLine();
            if (".!?".indexOf(s.charAt(s.length() - 1)) == -1) {
                System.out.println("Invalid Input! Sentence should start with upper case letter and end with full stop.");
            }
        } while (".!?".indexOf(s.charAt(s.length() - 1)) == -1);
        StringTokenizer st = new StringTokenizer(s, " ", false);
        while (st.hasMoreElements()) {
            System.out.println(st.nextElement());
        }
    }
}
